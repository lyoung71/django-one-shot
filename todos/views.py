from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateTodoForm
# Create your views here.

def todo_list_list (request):
    list = TodoList.objects.all()
    context = {
        "list_objects": list
    }
    return render(request,"todos/list.html",context)

def todo_list_detail (request, id):
    list_details = get_object_or_404(TodoList, id=id)
    context = {
        "list_tasks": list_details
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = CreateTodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect ("todo_list_detail", id=list.id)
    else:
        form = CreateTodoForm
    context = {
        "form": form
    }
    return render (request, "todos/create.html", context)

def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateTodoForm(request.POST, instance = list )
        if form.is_valid():
            form.save()
            return redirect ("todo_list_detail", id = id )
    else:
        form = CreateTodoForm(instance = list)
    context = {
        "list": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete (request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todos/list.html")

    return render(request, "todos/delete.html" )
