from django.forms import ModelForm
from todos.models import TodoList


class CreateTodoForm (ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name"
        ]
